<?php
namespace Egoi;

return array(
    'router' => array(
        'routes' => array(
            'egoi-hello-world' => array(
                'type'    => 'Zend\Mvc\Router\Http\Literal',
                    'options' => array(
                    'route' => '/hello/world',
                    'defaults' => array(
                        'controller' => 'Egoi\Controller\Hello',
                        'action'     => 'world',
                    ),
                ),
            ),
            'egoi-category' => array(
                'type'    => 'Literal',
                    'options' => array(
                    'route' => '/restful',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Egoi\Controller',
                        'controller'    => 'Restful',
                    ),
                ),
            ),
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/restful',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Egoi\Controller',
                        'controller'    => 'Restful',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                    'client' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/client[/:action]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Client',
                                'action'     => 'index'
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Egoi\Controller\Hello' => 'Egoi\Controller\HelloController',
            'Egoi\Controller\Client' => 'Egoi\Controller\ClientController'
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Egoi' => __DIR__ . '/../view'
        ),
    ),
    // ... other configuration ...
);