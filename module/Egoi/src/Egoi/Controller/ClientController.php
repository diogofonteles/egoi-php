<?php

namespace Egoi\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\Client as HttpClient;
use Zend\View\Model\JsonModel;

class ClientController extends AbstractActionController
{
    public function indexAction()
    {
        // Allow from any origin
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

            exit(0);
        }

        // header("Access-Control-Allow-Origin: *");
        // header('Access-Control-Allow-Headers: X-Requested-With');

        // header("Access-Control-Allow-Methods: ", "GET, POST, OPTIONS, PUT, DELETE");

        $pathJson = 'C:\dev\php\egoi-api\module\Egoi\data\category.json';
        $contents = file_get_contents($pathJson);
        $json = json_decode($contents, true);
        $methodType = $this->params()->fromQuery('method', 'get');
        $id = $this->params()->fromQuery('id');
        $method = $_SERVER['REQUEST_METHOD'];

        header('Content-type: application/json');
        $body = file_get_contents('php://input');

        function findById($vector, $id)
        {
            $found = -1;
            foreach ($vector as $key => $obj) {
                if ($obj['id'] == $id) {
                    $found = $key;
                    break;
                }
            }

            return $found;
        }

        if ($methodType === 'get-list' && $id) {
            echo 'Error. You cannot pass a parameter in this request.';
            exit;
        }

        if ($methodType === 'get' && !$id) {
            echo 'Error. you need to pass the ID parameter in this request.';
            exit;
        }

        switch ($methodType) {
            case 'get':
                if ($method === 'GET') {
                    $found = findById($json, $id);

                    if ($found >= 0) {
                        echo json_encode($json[$found]);
                    } else {
                        echo 'ERROR!';
                        exit;
                    }
                }
                break;
            case 'get-list':
                if ($method === 'GET') {
                    if ($json) {
                        echo json_encode($json);
                    } else {
                        echo '[]';
                    }
                }
                break;
            case 'create':
                if ($method === 'POST') {
                    $jsonBody = json_decode($body, true);
                    $jsonBody['id'] = time();

                    $json[] = $jsonBody;
                    echo json_encode($jsonBody);
                    file_put_contents($pathJson, json_encode($json));
                }

                break;
            case 'update':
                if (!$id) {
                    echo 'ERROR!';
                }

                if ($method === 'PUT') {
                    $found = findById($json, $id);

                    if ($found >= 0) {
                        $jsonBody = json_decode($body, true);
                        $jsonBody['id'] = $id;
                        $json[$found] = $jsonBody;
                        echo json_encode($json[$found]);
                        file_put_contents($pathJson, json_encode($json));
                    } else {
                        echo 'ERROR!';
                        exit;
                    }
                }
                break;
            case 'delete':
                if (!$id) {
                    echo 'ERROR!';
                }

                if ($method === 'DELETE') {
                    $found = findById($json, $id);

                    if ($found >= 0) {
                        echo json_encode($json[$found]);
                        unset($json[$found]);
                        file_put_contents($pathJson, json_encode($json));
                    } else {
                        echo 'ERROR!';
                        exit;
                    }
                }
                break;
        }


        $response = $this->getResponse();
        $response->setContent($body);

        return $response;
    }
}
