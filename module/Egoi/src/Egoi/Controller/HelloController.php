<?php
namespace Egoi\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class HelloController extends AbstractActionController
{
    public function worldAction()
    {
        if (isset($_REQUEST)) {
            var_dump($_REQUEST);
        }
        
        $message = $this->params()->fromQuery('message', 'foo');
        echo "<script>console.log('Debug Objects: " . $message . "' );</script>";
        return new ViewModel(array('message' => $message));
    }
}